{pkgs, ...}: {
  programs.waybar = {
    enable = true;
    package = pkgs.waybar;
    style = ./style.css;
    settings = [
      {
        height = 30;
        layer = "top";
        position = "top";
        margin-left = 10;
        margin-right = 10;
        margin-top = 10;

        # Modules positions

        modules-left = [
          "hyprland/workspaces"
        ];

        modules-center = [
          "hyprland/window"
        ];

        modules-right = [
          "network"
          #"bluetooth"
          "pulseaudio"
          "backlight"
          "disk"
          "memory"
          "cpu"
          "battery"
          "clock"
        ];

        # Modules

        disk = {
          format = " {percentage_used}%";
          tooltip = "Free: {free} GB, Used: {used} GB, Total: {total} GB";
        };

        battery = {
          interval = 10;
          states = {
            full = 95;
            warning = 30;
            critical = 15;
          };
          format = " {icon} {capacity}%";
          format-discharging = "{icon} {capacity}%";
          format-icons = [
            " "
            " "
            " "
            " "
            " "
          ];
          tooltip = true;
        };

        memory = {
          format = "  {used}/{total}";
          tooltip = "In use: {percentage}%, Swap: {swapPercentage}%";
        };

        cpu = {
          format = "  {usage}%";
          tooltip = "Avg: {avg_frequency}, Max: {max_frequency}, Min: {min_frequency}";
        };

        clock = {
          format = "{:%H:%M | %d/%m/%Y}";
        };

        network = {
          interval = 5;
          format-wifi = "  {essid}";
          format-disconnected = "󱛅  Disconnected";
          format-disabled = "󰖪  Disabled";
          format-alt = " : {bandwidthUpBits} |  : {bandwidthDownBits}";
          tooltip-format = "{ifname} via {ipaddr}/{cidr}";
        };

        backlight = {
          format = "󰳲 : {}%";
        };

        pulseaudio = {
          format = "{icon} {volume}%";
          format-muted = " ";
          format-bluetooth = " {volume}% {format_source}";
          format-bluetooth-muted = " Mute";
          format-source = " {volume}%";
          format-source-muted = " ";
          format-icons = {
            default = [
              ""
              ""
              " "
            ];
          };
        };

        "hyprland/workspaces" = {
          format = "<b>{id}</b>";
        };

        "hyprland/window" = {
          format = "{title} - {class}";
          rewrite = {
            "(.*) - kitty" = "  kitty";
            "(.*) - chromium-browser" = " Chromium";
            "(.*) - firefox" = "󰈹 Firefox";
            " - " = " NixOS";
          };
        };
      }
    ];
  };
}
