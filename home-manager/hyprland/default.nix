{
  config,
  lib,
  pkgs,
  inputs,
  ...
}: {
  imports = [
    ./waybar
  ];

  home = {
    packages = with pkgs; [
      wl-mirror
      wpaperd
      brightnessctl
      bluetuith
      feh
      grim
      slurp
      ranger
      sioyek
      hyprlock
    ];

    #home.file.".config/hypr/hyprpaper.conf".source = ./conf/hyprpaper.conf;
    file = {
      ".config/wpaperd/wallpaper.toml".source = ./conf/wallpaper.toml;
      ".config/hypr/scripts".source = ./scripts;
      ".config/hypr/hyprlock.conf".source = ./conf/hyprlock.conf;
    };
  };

  programs.kitty = {
    enable = true;
    theme = "Brogrammer";
    extraConfig = "background_opacity 0.65";
    font = {
      name = "Julia Mono";
    };
  };

  programs.anyrun = {
    enable = true;
    config = {
      plugins = with inputs.anyrun.packages.${pkgs.system}; [
        applications
        rink
        shell
        symbols
      ];
      width.fraction = 0.3;
      y.absolute = 15;
      hidePluginInfo = true;
      closeOnClick = true;
    };
    extraCss = builtins.readFile conf/anyrun.css;
  };

  services.dunst = {
    enable = true;
  };

  wayland.windowManager.hyprland = {
    enable = true;
    extraConfig = ''
      env = WLR_DRM_NO_ATOMIC,1

      $mod = SUPER
      $volume = ~/.config/hypr/scripts/volume

      exec-once = wpaperd
      exec-once = waybar

      monitor = eDP-1,preferred,auto,1.2

      input {
        follow_mouse = 1
        natural_scroll = 0
        force_no_accel = 0
      }

      general {
        gaps_in = 7
        gaps_out = 7

        border_size = 1

        #allow_tearing = true
      }

      decoration {
        rounding = 16
        #drop_shadow = false
        blur {
          enabled = true
          size = 10
          noise = 0.1
        }
      }

      animations {
        enabled = true
      }

      xwayland {
        force_zero_scaling = true
      }

      misc {
        disable_hyprland_logo = true
        vrr = 0
      }

      # Basics
      bind = $mod, RETURN, exec, kitty
      bind = $mod, Q, killactive,
      bind = $mod, U, fullscreen,0 #Fullscreen
      bind = $mod, I, fullscreen,1 #Maximize window
      bind = $mod, O, toggleopaque,
      bind = $mod, F, togglefloating,
      bind = $mod SHIFT, M, exec, pkill Hyprland
      bind = $mod SHIFT, L, exec, hyprlock
      bind = ,XF86Sleep, exec, hyprlock& systemctl suspend
      bindl = ,switch:on:Lid Switch, exec, hyprlock& systemctl suspend

      # Screenshot
      bind = ,Print,exec,grim /home/ahmed/Pictures/Screenshots/$(date +'%s_grim.png') && dunstify grim 'Screenshot saved'
      bind = $mod,Print,exec,grim -g "$(slurp)" /home/ahmed/Pictures/Screenshots/$(date +'%s_grim.png') && dunstify grim 'Partial Screenshot saved'

      # App launcher (anyrun)
      bind = $mod ALT, down, exec, anyrun

      # move focus
      bind = $mod,  left, movefocus, l
      bind = $mod, right, movefocus, r
      bind = $mod,    up, movefocus, u
      bind = $mod,  down, movefocus, d

      # move window
      bind = $mod SHIFT,  left, movewindow, l
      bind = $mod SHIFT, right, movewindow, r
      bind = $mod SHIFT,    up, movewindow, u
      bind = $mod SHIFT,  down, movewindow, d

      # window resize
      bind = $mod, S, submap, resize

      submap = resize
      binde = ,right,resizeactive,10 0
      binde = ,left,resizeactive,-10 0
      binde = ,up,resizeactive,0 10
      binde = ,down,resizeactive,0 -10
      bind  = ,escape,submap,reset
      submap = reset

      # Function keys
      bind=,XF86MonBrightnessUp,exec,brightnessctl s +2%
      bind=,XF86MonBrightnessDown,exec,brightnessctl s 2%-
      bind=,XF86AudioRaiseVolume,exec,$volume --inc
      bind=,XF86AudioLowerVolume,exec,$volume --dec
      bind=,XF86AudioMute,exec,$volume --toggle
      bind=,XF86AudioMicMute,exec,$volume --toggle-mic

      # Workspaces
      ${builtins.concatStringsSep "\n" (builtins.genList (
          x: let
            ws = let
              c = (x + 1) / 10;
            in
              builtins.toString (x + 1 - (c * 10));
          in ''
            bind = $mod, ${ws}, workspace, ${toString (x + 1)}
            bind = $mod SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}
            bind = $mod CTRL, ${ws}, movetoworkspacesilent, ${toString (x + 1)}
          ''
        )
        10)}

      bind = $mod ALT, left, workspace, e-1
      bind = $mod ALT, right, workspace, e+1

      #blurls  = waybar
    '';
  };

  fonts.fontconfig.enable = true;

  home.pointerCursor = {
    gtk.enable = true;
    package = pkgs.bibata-cursors;
    name = "Bibata-Modern-Ice";
    size = 16;
  };

  gtk = {
    enable = true;
    cursorTheme = {
      package = pkgs.bibata-cursors;
      name = "Bibata-Modern-Ice";
      size = 16;
    };
  };
}
