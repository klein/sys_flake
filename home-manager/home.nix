# This is your home-manager configuration file
# Use this to configure your home environment (it replaces ~/.config/nixpkgs/home.nix)
{
  inputs,
  lib,
  config,
  pkgs,
  ...
}: let
  neovimconfig = import ../nixvim;
  nvim = inputs.nixvim.legacyPackages.x86_64-linux.makeNixvimWithModule {
    inherit pkgs;
    module = neovimconfig;
  };
in {
  # You can import other home-manager modules here
  imports = [
    inputs.hyprland.homeManagerModules.default
    inputs.anyrun.homeManagerModules.default
    inputs.nixvim.homeManagerModules.nixvim
    ./hyprland
  ];

  home = {
    username = "ahmed";
    homeDirectory = "/home/ahmed";
  };

  home.packages = with pkgs; [
    # Web/Online stuff
    firefox
    ungoogled-chromium

    # Rust
    rustup
    gcc
    cargo-generate

    # Office
    hunspell
    hunspellDicts.en_US
    hunspellDicts.fr-moderne
    typst

    # Dev
    tmux
    devenv
    just

    # IDE
    nvim
    jetbrains.datagrip

    # Languages
    scala
    nodejs_18
    go
    julia_19-bin

    # Package managers
    sbt
    poetry

    # DS Tools
    graphviz
    jq
    sq

    # DE Tools
    pgcli

    # Iot
    mqttx

    (python311.withPackages (ps:
      with ps; [
        ###### LSP Server #####
        python-lsp-server
        ###### DS libs ######
        # basics
        pandas
        numpy
        jupyter
        pillow
        sympy
        # Plotting
        matplotlib
        seaborn
        # ML
        scikit-learn
        xgboost

        ########## Networking ######
        requests
        scapy

        ######### Utils #########
        pycryptodome
        diagrams
        pwntools
        pyx
      ]))
  ];

  #programs.nixvim.enable = true;
  programs.git.enable = true;

  programs.zsh = {
    enable = true;
    initExtra = ''export PATH=$PATH":/home/ahmed/go/bin"'';
    oh-my-zsh = {
      enable = true;
      theme = "robbyrussell";
      plugins = [];
    };
  };

  programs.vscode = {
    enable = true;
    package = pkgs.vscode.fhs;
    extensions = with pkgs.vscode-extensions; [
      dracula-theme.theme-dracula
      rust-lang.rust-analyzer
    ];
  };

  # Nicely reload system units when changing configs
  systemd.user.startServices = "sd-switch";
  home.enableNixpkgsReleaseCheck = false;
  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  home.stateVersion = "24.05";
}
