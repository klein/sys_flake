echo "[+] Working on it..."
if nix flake check
then
	echo "[+] Check passed committing..."
	git add .
	git commit -m "..."
	echo "[+] Applying flake..."
	sudo nixos-rebuild switch --flake .#dell
	echo "[+] Done !"
else
	echo "[!] Something is wrong with the flake... Fix it to proceed"
fi
