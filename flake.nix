{
  description = "Holy NixOS config";

  inputs = {
    # Nixpkgs
    #nixpkgs.url = "github:nixos/nixpkgs/nixos-24.05";
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    # Home manager
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";

    # Hyprland
    hyprland.url = "git+https://github.com/hyprwm/Hyprland?submodules=1";

    # anyrun
    anyrun.url = "github:Kirottu/anyrun";

    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    #NixVim
    nixvim = {
      url = "github:nix-community/nixvim";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    self,
    nixpkgs,
    home-manager,
    rust-overlay,
    ...
  } @ inputs: let
    inherit (self) outputs;
    pkgs = import nixpkgs {
      system = "x86_64-linux";
      config.allowUnfree = true;
      #overlays = [rust-overlay.overlays.default];
    };
  in {
    # NixOS configuration entrypoint
    # Available through 'nixos-rebuild --flake .#your-hostname'
    nixosConfigurations = {
      dell = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        specialArgs = {inherit inputs outputs pkgs;};
        modules = [
          # NixOS config file
          ./nixos/configuration.nix

          {programs.hyprland = {enable = true;};}

          ({pkgs, ...}: {
            #            environment.systemPackages = [ pkgs.rust-bin.stable.latest.default ];
          })

          # Home Manager settings
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.extraSpecialArgs = {inherit inputs outputs pkgs;};
            home-manager.users.ahmed = import ./home-manager/home.nix;
          }
        ];
      };
    };
  };
}
