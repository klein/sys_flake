{pkgs, ...}: {
  config = {
    colorschemes.oxocarbon = {
      enable = true;
    };

    plugins = {
      notify.enable = true;

      lualine = {
        enable = true;
        iconsEnabled = true;
        globalstatus = true;
        theme = "auto";
      };

      noice = {
        enable = true;

        presets = {
          bottom_search = true;
        };
      };
    };

    extraConfigLua = ''
       vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
       vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
      require("notify").setup({
      background_colour = "#000000",
      })
    '';
  };
}
