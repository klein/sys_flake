{pkgs, ...}: {
  config.plugins = {
    cmp = {
      enable = true;
      settings = {
        autoEnableSources = true;
        experimental = {ghost_text = true;};
        snippet.expand = "function(args) require('luasnip').lsp_expand(args.body) end";
        mapping = {
          __raw = ''
              cmp.mapping.preset.insert({
            ['<Tab>'] = cmp.mapping(cmp.mapping.select_next_item(), {'i', 's'}),
              	['<C-d>'] = cmp.mapping.scroll_docs(-4),
              	['<C-f>'] = cmp.mapping.scroll_docs(4),
              	['<C-e>'] = cmp.mapping.abort(),
              	['<C-space>'] = cmp.mapping.complete(),
                ['<S-CR>'] = cmp.mapping.confirm({ select = true }),
              })
          '';
        };
        sources = [
          {name = "nvim_lsp";}
          {
            name = "buffer";
            options.get_bufnrs.__raw = "vim.api.nvim_list_bufs";
            keywordLength = 3;
          }
          {
            name = "path";
            keywordLength = 3;
          }
          {
            name = "luasnip";
            keywordLength = 3;
          }
        ];
        window = {
          completion = {border = "solid";};
        };
      };
    };

    cmp-buffer.enable = true;
    cmp-nvim-lsp.enable = true;
    cmp-nvim-lua.enable = true;
    cmp-cmdline.enable = true;
    cmp-path.enable = true;
    luasnip.enable = true;
  };

  config.extraPackages = with pkgs; [
    alejandra
  ];
}
