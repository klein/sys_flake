{
  pkgs,
  lib,
  ...
}: {
  imports = [
    ./lsp/lsp.nix
    ./lsp/lspsaga.nix
    ./lsp/none-ls.nix
    ./lint.nix
    ./formatter.nix
    ./style.nix
    ./completion.nix
    ./telescope.nix
    ./git.nix
    ./ui.nix
  ];

  config = {
    globals = {
      mapleader = " ";
    };

    keymaps = [
      {
        action = "<cmd>w<CR>";
        key = "<C-s>";
      }
    ];

    opts = {
      number = true;
      relativenumber = true;

      shiftwidth = 2;
      colorcolumn = "80";
      tabstop = 2;

      smartindent = true;

      breakindent = true;

      wrap = true;

      mouse = "a";

      updatetime = 50;
      completeopt = ["menuone" "noselect" "noinsert"];

      swapfile = false;
      backup = false;
      undofile = false;

      termguicolors = true;
      cursorline = true;
      scrolloff = 8;
    };
  };
}
