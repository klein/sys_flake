{
  config = {
    plugins = {
      lsp-format = {enable = true;};

      lsp = {
        enable = true;
        servers = {
          nil-ls.enable = true;
          pylsp.enable = true;
          sqls.enable = true;
          html.enable = true;
          cssls.enable = true;
          tsserver.enable = true;
          bashls.enable = true;
          rust-analyzer = {
            enable = true;
            installRustc = false;
            installCargo = false;
          };
        };
      };

      fidget = {
        enable = true;
        logger = {
          level = "info";
          floatPrecision = 0.01;
        };
        progress = {
          pollRate = 0;
          suppressOnInsert = true;
          ignoreDoneAlready = false;
          ignoreEmptyMessage = false;
          clearOnDetach = ''
            function(client_id)
            	local client = vim.lsp.get_client_by_id(client_id)
            	return client and client.name or nil
            end
          '';
          notificationGroup = ''
            function(msg) return msg.lsp_client.name end
          '';
          ignore = [];
          lsp = {
            progressRingbufSize = 0;
          };
          display = {
            renderLimit = 16;
            doneTtl = 3;
            doneStyle = "Constant";
            progressTtl = "math.huge";
            progressIcon = {
              pattern = "dots";
              period = 1;
            };
            progressStyle = "WarningMsg";
            groupStyle = "Title";
            iconStyle = "Question";
            priority = 30;
            skipHistory = true;
          };
        };

        notification = {
          pollRate = 10;
          historySize = 128;
          overrideVimNotify = true;
          window = {
            normalHl = "Comment";
            winblend = 0;
            border = "none";
            zindex = 45;
            align = "bottom";
            relative = "editor";
          };

          view = {
            stackUpwards = true;
            groupSeparator = "---";
            groupSeparatorHl = "Comment";
          };
        };
      };

      trouble = {
        enable = true;
      };
    };

    extraConfigLua = ''
      local _border = "rounded"

      vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(
      	vim.lsp.handlers.hover, { border = _border }
      )

      vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(
      	vim.lsp.handlers.signature_help, { border = _border }
      )
    '';
  };
}
