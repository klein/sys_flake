{
  plugins.lspsaga = {
    enable = true;
    beacon = {enable = true;};
    ui = {
      border = "rounded";
      codeAction = "💡";
    };
    hover = {
      openCmd = "!firefox";
      openLink = "gx";
    };
    diagnostic = {
      borderFollow = true;
      diagnosticOnlyCurrent = false;
      showCodeAction = true;
    };
    symbolInWinbar = {enable = true;};
    codeAction = {
      extendGitSigns = true;
      showServerName = true;
      onlyInCursor = true;
      numShortcut = true;
      keys = {
        exec = "<CR>";
        quit = ["<Esc>" "q"];
      };
    };
    lightbulb = {
      enable = false;
      sign = false;
      virtualText = true;
    };
  };
}
