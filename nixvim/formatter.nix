{pkgs, ...}: {
  config.plugins.conform-nvim = {
    enable = true;
    formattersByFt = {
      nix = ["alejandra"];
    };

    formatOnSave = {
      lspFallback = true;
      timeoutMs = 2000;
    };
  };
}
