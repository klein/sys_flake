{pkgs, ...}: {
  plugins.git-worktree = {
    enable = true;
    enableTelescope = true;
  };

  plugins.gitlinker = {
    enable = true;
    callbacks = {
      "github.com" = "get_github_type_url";
    };
  };

  extraPackages = with pkgs; [
    lazygit
  ];

  extraPlugins = with pkgs.vimPlugins; [
    lazygit-nvim
  ];

  extraConfigLua = ''require("telescope").load_extension("lazygit")'';

  keymaps = [
    {
      mode = "n";
      key = "<leader>gg";
      action = "<cmd>LazyGit<CR>";
      options = {
        desc = "LazyGit (root dir)";
      };
    }
  ];
}
